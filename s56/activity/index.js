/**TODO:
 * Check if letter is a single character
 * Initialize a counter for each letter
 * Loop over the sentence
 * Initialize the current iteration to a variable letter
 * Check if letter is equal to the current letter in the sentence
 * If yes, increment the counter for the current letter
 * Return the counter for the letter
 */
function countLetter(letter, sentence) {
  // Check first whether the letter is a single character.
  if (letter.length !== 1) {
    return undefined;
  }

  // If letter is a single character, count how many times a letter has occurred in a given sentence then return result
  let result = 0;
  for (let i = 0; i < sentence.length; i++) {
    // Check if letter is equal to the current letter in the sentence.
    if (letter === sentence[i]) {
      result++;
    }
  }
  return result;
  // If letter is invalid, return undefined.
}

/**TODO:
 * Ignore case by turning text to lowercase
 * Initialize a counter for each letter
 * Loop over the text
 * Initialize the current iteration to a variable letter
 * Check and ignore if letter is a space
 * Check if letter is already in the frequency counter
 * Return false if
 */
function isIsogram(text) {
  // An isogram is a word where there are no repeating letters.
  // The function should disregard text casing before doing anything else.

  text = text.toLowerCase();

  // Initialize a boolean counter for each letter.
  let frequency = {};

  for (let i = 0; i < text.length; i++) {
    const letter = text[i];

    if (letter === " ") {
      continue; // Ignore spaces
    }
    if (frequency[letter]) {
      // If the function finds a repeating letter, return false.
      return false;
    } else {
      // Else, set the current letter to true
      frequency[letter] = true;
    }
  }
  // If the function does not find any repeating letter, return true.
  return true;
}

/**TODO:
 * Check if age is less than 13
 * Return undefined for people aged below 13.
 * Check if age is between 13 and 21 or age is greater than 64
 * Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
 * Return the rounded off price for people aged 22 to 64.
 */
function purchase(age, price) {
  // Return undefined for people aged below 13.
  if (age < 13) {
    return undefined;
  }
  // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
  else if ((age >= 13 && age <= 21) || age >= 64) {
    // round off to five decimals
    return (price * 0.8).toFixed(2);
  }
  // Return the rounded off price for people aged 22 to 64.
  else {
    return price.toFixed(2);
  }
  // The returned value should be a string.
}

/**TODO:
 * Initialize an empty array for hotCategories
 * Loop over the items array
 * Check if the current element in the items array has no more stocks
 * Check if the current element in the items array is already in the hotCategories array
 * Push the current element in the items array to the hotCategories array
 * Return the hotCategories array
 */
function findHotCategories(items) {
  const hotCategories = [];

  for (let i = 0; i < items.length; i++) {
    //   Find categories that has no more stocks.
    if (items[i].stocks === 0 && !hotCategories.includes(items[i].category)) {
      // Check if the category is already in the array
      hotCategories.push(items[i].category);
    }
  }
  return hotCategories;

  // The hot categories must be unique; no repeating categories.
  // The passed items array from the test are the following:
  // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
  // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
  // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
  // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
  // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
  // The expected output after processing the items array is ['toiletries', 'gadgets'].
  // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
}

/**TODO:
 * Initialize an empty array for flyingVoters
 * Loop over the first array
 * Loop over the second array
 * Check if the current element in the first array is equal to the current element in the second array
 * If yes, push the current element in the first array to the flyingVoters array
 * Return the flyingVoters array
 */
function findFlyingVoters(candidateA, candidateB) {
  const flyingVoters = [];
  // Find voters who voted for both candidate A and candidate B.
  for (let i = 0; i < candidateA.length; i++) {
    for (let j = 0; j < candidateB.length; j++) {
      if (candidateA[i] === candidateB[j]) {
        flyingVoters.push(candidateA[i]);
      }
    }
  }
  return flyingVoters;

  // The passed values from the test are the following:
  // candidateA: ["LIWf1l", "V2hjZH", "rDmZns", "PvaRBI", "i7Xw6C", "NPhm2m"];
  // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l'];
  // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
  // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
}

module.exports = {
  countLetter,
  isIsogram,
  purchase,
  findHotCategories,
  findFlyingVoters,
};
