import Container from "react-bootstrap/Container";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";

import { Link, NavLink } from "react-router-dom";
import { useContext } from "react";
import UserContext from "../UserContext";

// The as keyword allows us to use the NavLink component as a Nav.Link component
// This is because the Nav.Link component is a wrapper for the NavLink component
// The to keyword is the same as the href keyword in the anchor tag
export default function AppNavBar() {
  // Capture the value from localStorage and set it to the user state

  const { user } = useContext(UserContext);

  return (
    <Navbar bg="primary" data-bs-theme="dark" expand="lg">
      <Container fluid>
        <Navbar.Brand as={Link} to="/">
          Zuitt Booking
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto">
            <Nav.Link as={NavLink} to="/">
              Home
            </Nav.Link>
            <Nav.Link as={NavLink} to="/courses">
              Courses
            </Nav.Link>
            {user.id === null || user.id === undefined ? (
              <>
                <Nav.Link as={NavLink} to="/register">
                  Register
                </Nav.Link>
                <Nav.Link as={NavLink} to="/login">
                  Login
                </Nav.Link>
              </>
            ) : (
              <Nav.Link as={NavLink} to="/logout">
                Logout
              </Nav.Link>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
