import { Row, Col, Card, Button } from "react-bootstrap";
import { useParams } from "react-router-dom";
import { useState, useEffect } from "react";

import Swal2 from "sweetalert2";

export default function CourseView() {
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);

  const { id } = useParams();

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/courses/${id}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((data) => {
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
      });
  }, [id]);

  const enroll = (courseId) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/enroll`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        courseId: `${courseId}`,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data === true) {
          Swal2.fire({
            title: "Enrollment successful!",
            icon: "success",
            text: "You are now enrolled in the course!",
          });
        } else {
          Swal2.fire({
            title: "Enrollment unsuccessful",
            icon: "error",
            text: "Please try again!",
          });
        }
      });
  };

  return (
    <Row>
      <Col>
        <Card className="mt-5">
          <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Text>{description}</Card.Text>
            <Card.Text>Price: {price}</Card.Text>
            <Button variant="primary" onClick={() => enroll(id)}>
              Enroll
            </Button>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
