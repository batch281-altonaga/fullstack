import React from "react";

// Create a context object
const UserContext = React.createContext();

// Export it so it can be used by other Components
export default UserContext;

// Create a provider for components to consume and subscribe to changes
export const UserProvider = UserContext.Provider;
