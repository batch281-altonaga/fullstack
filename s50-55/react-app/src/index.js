import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";

import "bootstrap/dist/css/bootstrap.min.css";

// createRoot assigns the elements to be managed by React at the root of the virtual DOM.
const root = ReactDOM.createRoot(document.getElementById("root"));

// render is the method that actually renders the React component.
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// Alternative:
/* 
ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("app")
); 
*/
