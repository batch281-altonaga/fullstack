import { useEffect, useState, useContext } from "react";
import { Row, Col, Button, Form } from "react-bootstrap";
import { Navigate, useNavigate } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function Register() {
  const [email, setEmail] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [mobileNo, setMobileNo] = useState("");
  const [password, setPassword] = useState("");
  const [password2, setPassword2] = useState("");

  const [isEmailValid, setIsEmailValid] = useState(false);
  const [isDisabled, setIsDisabled] = useState(false);
  const [isPasswordMatch, setIsPasswordMatch] = useState(true);

  const { user } = useContext(UserContext);

  const navigate = useNavigate();

  useEffect(() => {
    if (email.length <= 20) {
      return setIsEmailValid(false);
    } else {
      return setIsEmailValid(true);
    }
  }, [email]);

  useEffect(() => {
    if (
      email !== "" &&
      password !== "" &&
      password2 !== "" &&
      email.length <= 20 &&
      password === password2
    ) {
      return setIsDisabled(false);
    } else {
      return setIsDisabled(true);
    }
  }, [email, password, password2]);

  // S55 Activity
  function registerUser(e) {
    e.preventDefault();

    // Check if user already exists
    fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ email }),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data === true) {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Email already exists!",
          });
        } else {
          // Register the user
          fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              firstName,
              lastName,
              email,
              password,
              mobileNo,
            }),
          })
            .then((response) => response.json())
            .then((data) => {
              Swal.fire({
                icon: "success",
                title: "Success!",
                text: "You have successfully registered!",
              });
              navigate("/login");
            });
        }
      });
  }

  useEffect(() => {
    if (password === password2) {
      return setIsPasswordMatch(true);
    } else {
      return setIsPasswordMatch(false);
    }
  }, [password, password2]);

  return (
    <>
      {user.id === null || user.id === undefined ? (
        <Row className="mt-5" border="light">
          <Col className="col-6 mx-auto">
            <h1>Register</h1>
            <Form onSubmit={registerUser}>
              <Form.Group className="mb-3" controlId="formFirstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter first name"
                  value={firstName}
                  onChange={(e) => setFirstName(e.target.value)}
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="formLastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter last name"
                  value={lastName}
                  onChange={(e) => setLastName(e.target.value)}
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control
                  type="email"
                  placeholder="Enter email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
                <Form.Text className="text-muted" hidden={isEmailValid}>
                  Please enter a valid email address
                </Form.Text>
              </Form.Group>

              <Form.Group className="mb-3" controlId="formMobileNo">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter mobile no."
                  value={mobileNo}
                  onChange={(e) => setMobileNo(e.target.value)}
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="formBasicPassword1">
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Enter password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="formBasicPassword2">
                <Form.Label>Confirm Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Enter password again"
                  value={password2}
                  onChange={(e) => setPassword2(e.target.value)}
                />
                <Form.Text className="text-danger" hidden={isPasswordMatch}>
                  Password does not match
                </Form.Text>
              </Form.Group>

              <Button variant="primary" type="submit" disabled={isDisabled}>
                Sign Up
              </Button>
            </Form>
          </Col>
        </Row>
      ) : (
        <Navigate to="/*" />
      )}
    </>
  );
}
