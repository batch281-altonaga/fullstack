import { Col, Container, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import img from "./error.jpg";

export default function NotFound() {
  const error404 = {
    alt: "404 Error",
    width: 400,
  };
  return (
    <Container>
      <Row>
        <Col className="d-flex flex-column align-items-center justify-content-center p-5">
          <h1>Page Not Found</h1>
          <img src={img} alt={error404.alt} width={error404.width} />
          <h4>
            Go back to the{" "}
            <Link to="/" className="text-decoration-none">
              homepage
            </Link>
            .
          </h4>
        </Col>
      </Row>
    </Container>
  );
}
