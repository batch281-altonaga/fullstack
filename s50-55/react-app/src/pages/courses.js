import { Fragment, useEffect, useState } from "react";
import CourseCard from "../components/CourseCard.js";

export default function Courses() {
  const [coursesData, setCoursesData] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/courses/`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((data) => {
        setCoursesData(
          data.map((course) => {
            return <CourseCard key={course._id} course={course} />;
          })
        );
      });
  }, []);

  return <Fragment>{coursesData}</Fragment>;
}
