const txtFirstName = document.querySelector("#txt-first-name");
const spanFullName = document.querySelector("#span-full-name");
txtFirstName.addEventListener("keyup", (event) => {
  spanFullName.innerHTML = txtFirstName.value;
});
txtFirstName.addEventListener("keyup", (event) => {
  console.log(event.target);
  console.log(event.target.value);
});

// Higher-order functions
// - a higher-order function is a function that can accept one or more functions as arguments
// - or return a function as its result

// Callback functions
// - a callback function is a function that is passed to another function as an argument
// - and is executed after some operation has been completed
