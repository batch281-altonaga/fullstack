const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

const updateFirstName = (event) => {
  spanFullName.innerHTML = txtFirstName.value;
};

const updateLastName = (event) => {
  spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
};

txtFirstName.addEventListener("keyup", updateFirstName);

txtLastName.addEventListener("change", updateLastName);
